# Cheil Frontend

Este desarrollo fue realizado en la herramienta Visual Studio Code
y se utilizaron los Frameworks:
- Angular 15
- Angular Material (para el componente de Grilla)
- FortAwesome - Angular Icons
- Ngx Toastr (Para las notificaciones)

## Configuración de EndPoint
Se debe configurar la URL del Backend realizado en NetCore (environment.ts)

## Requerimientos
- Node JS v 18

## Ejecución
Se debe clonar este repositorio desde Visual Studio Code
- y luego ejecutar por consola el comando: npm install (para instalar todas las dependencias)

## Demostración
http://sealy-test.ddns.net:4200/cheilhotel/
