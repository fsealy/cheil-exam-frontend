export interface Hotel {
  hotelId: number;
  hotelName : string;
  category: number;
  price: number;
  picture1: string;
  picture2: string;
  picture3: string;
  hotelDesc: string;
  scorePoint: number;
}
