import { environment } from './../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Hotel } from '../model/hotel';
import { Subject, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  constructor(private http: HttpClient) {
  }

  public updater = new Subject<void>();

  getHotels() {
    let url = environment.url + '/hotel/getAll';
    return this.http.get<Hotel[]>(url);
  }

  orderByPrice(order: string) {
    let url = environment.url + '/hotel/orderByPrice?order=' + order;
    return this.http.get<Hotel[]>(url).pipe(tap(() => {
      this.updater.next();
    }));;
  }

  getByFilters(category:number, qualify:number) {
    let url = environment.url + '/hotel/getByFilters?category=' + category + '&qualify=' + qualify;
    return this.http.get<Hotel[]>(url).pipe(tap(() => {
      this.updater.next();
    }));;
  }
}
