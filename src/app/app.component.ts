import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Hotel } from './model/hotel';
import { faArrowDown, faArrowUp, faFilter } from '@fortawesome/free-solid-svg-icons';
import { HotelService } from './services/hotel.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  faArrowDown = faArrowDown;
  faArrowUp = faArrowUp;
  faFilter = faFilter;
  order: string = "A";
  category: any = 0;
  qualify: any = 0;

  constructor(
    private service: HotelService,
    private toast: ToastrService
  ) {

  }

  list: Hotel[] = [
  ];

  ngOnInit(): void {
    this.service.getHotels().subscribe((res) => {
      this.list = res;
    });

    this.service.updater.subscribe((res) => {
    });
  }

  title = 'cheil-hotel';

  orderPrice() {
    if (this.order == 'A') {
      this.order = 'D';
      this.toast.success('Se ha establecido de ordenamiento principal, los hoteles con mayores precios.');
    } else {
      this.order = 'A';
      this.toast.success('Se ha establecido de ordenamiento principal, los hoteles con menores precios.');
    }
    this.service.orderByPrice(this.order).subscribe((res) => {
      this.list = res;
    });

  }

  filtrar() {
    this.service.getByFilters(this.category, this.qualify).subscribe((res) => {
      this.list = res;
      if (this.list.length == 0) {
        this.toast.warning("no se encontraron coincidencias para los filtros seleccionados.");
      }
    });
  }
}
